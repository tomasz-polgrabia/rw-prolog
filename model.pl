negation(bron_naladowana, nie_bron_naladowana).

possibly_executable([action1, actionn],[performer1, performern], pi).
always_executable([action1, actionn], [performer1, performern], pi).

possible_accessible(form2, form1).
always_accessible(form2, form1).
typically_accessible(form2, form1).

qalways(f1, [chown], [mietus]).
palways(f1, [chown], [mietus]).
talways(f1, [chown], [mietus]).

pinvolved([mietus], [action1, actionn], [performer1, performern]).
ainvolved([mietus], [action1, actionn], [performer1, performern]).
tinvolved([mietus], [action1, actionn], [performer1, performern]).


statement(a1, [ fredek_zyje, hador_ma_bron, bron_naladowana] ).
statement(a2, [ fredek_zyje, hador_ma_bron, nie_bron_naladowana] ).

formula(f1, [a1, a2]).

always( f1 ).

after( f1, [chown], [mietus]).
observable(f1, [chown], [mietus]).
causes(a, e, f1, g1).

typically_causes(a, e, f1, g1).

releases(a, e, f, pi).
noninertial(f).
preserves(a, e, f, pi).

state(initially, [ hador_ma_bron, bron_naladowana, nie_mietus_ma_bron]).
state(stateB, [ hador_ma_bron]).
state(stateC, [mietus]).
